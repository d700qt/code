# A description of what this class does
#
# @summary A short summary of the purpose of this class
#
# @example
#   include mymodule::myclass
class mymodule::myclass {
  file { 'c:/temp/test.txt' :
    ensure  => 'present',
    content => 'hello!',
  }
}
