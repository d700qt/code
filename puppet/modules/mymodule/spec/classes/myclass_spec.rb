require 'spec_helper'

describe 'mymodule::myclass' do
  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts }

      it { is_expected.to compile }
      # it { is_expected.to contain_package('7zip')
      #   .with(
      #     :ensure => 'present'
      #   )
      # }

      it {
        is_expected.to contain_file('c:/temp/test.txt')
        .with(
          :ensure => 'present',
        )
      }
    end
  end
end
