variable "subscription_id" {}
variable "client_id" {}
variable "client_secret" {}
variable "tenant_id" {}
variable "machine_username" {}
variable "machine_password" {}
variable "machine_name_prefix" {
    default = "adeweetman"
}
variable "machine_name_suffix" {
    default = "vm"
}
variable "machine_count" {
    default = "2"
}
