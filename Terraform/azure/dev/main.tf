/*
terraform {
  backend "local" {
    path = "terraform.tfstate"
  }
}
*/

# Run terraform init -backend-config="backend.private" to switch backend
terraform {
  backend "s3" {}
}

provider "azurerm" {
  subscription_id = "${var.subscription_id}"
  client_id       = "${var.client_id}"
  client_secret   = "${var.client_secret}"
  tenant_id       = "${var.tenant_id}"
}

resource "azurerm_resource_group" "rg_dev" {
  name     = "rg_dev"
  location = "West Europe"
}

resource "azurerm_network_security_group" "nsg_dev" {
  name                = "nsg_dev"
  location            = "${azurerm_resource_group.rg_dev.location}"
  resource_group_name = "${azurerm_resource_group.rg_dev.name}"

  security_rule {
    name                       = "WinRM"
    priority                   = 100
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "5985"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }

  security_rule {
    name                       = "RDP"
    priority                   = 101
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "3389"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
}


resource "azurerm_virtual_network" "vnet_dev" {
  name                = "vnet_dev"
  address_space       = ["10.0.0.0/16"]
  location            = "${azurerm_resource_group.rg_dev.location}"
  resource_group_name = "${azurerm_resource_group.rg_dev.name}"
}

resource "azurerm_subnet" "subnet_dev" {
  name                 = "subnet_dev"
  resource_group_name  = "${azurerm_resource_group.rg_dev.name}"
  virtual_network_name = "${azurerm_virtual_network.vnet_dev.name}"
  address_prefix       = "10.0.2.0/24"
}


resource "azurerm_network_interface" "vnic_dev" {
  count                     = "${var.machine_count}"
  name                      = "vnic_dev${count.index}"
  location                  = "${azurerm_resource_group.rg_dev.location}"
  resource_group_name       = "${azurerm_resource_group.rg_dev.name}"
  network_security_group_id = "${azurerm_network_security_group.nsg_dev.id}"

  ip_configuration {
    name                          = "ip_dev${count.index}"
    subnet_id                     = "${azurerm_subnet.subnet_dev.id}"
    private_ip_address_allocation = "dynamic"
  }
}

resource "azurerm_virtual_machine" "vm_dev" {
  count                 = "${var.machine_count}"
  name                  = "${var.machine_name_suffix}${count.index}"
  location              = "${azurerm_resource_group.rg_dev.location}"
  resource_group_name   = "${azurerm_resource_group.rg_dev.name}"
  network_interface_ids =  ["${element(azurerm_network_interface.vnic_dev.*.id, count.index)}"]
  vm_size               = "Standard_A1_v2"

  # Uncomment this line to delete the OS disk automatically when deleting the VM
  delete_os_disk_on_termination = true

  # Uncomment this line to delete the data disks automatically when deleting the VM
  delete_data_disks_on_termination = true

  storage_image_reference {
    publisher = "RedHat"
    offer     = "RHEL"
    sku       = "7.2"
    version   = "latest"
  }

  storage_os_disk {
    name              = "${var.machine_name_suffix}${count.index}_disk_os"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
  }

  os_profile {
    computer_name  = "${var.machine_name_prefix}-${var.machine_name_suffix}${count.index}"
    admin_username = "${var.machine_username}"
    admin_password = "${var.machine_password}"
  }

  os_profile_linux_config {
    disable_password_authentication = false
  }
  /*os_profile_windows_config {
    winrm = {
      protocol = "http"
    }
  }*/

  tags {
    environment = "${count.index == 0 ? "production" : "dev"}"
    #environment = "dev2"
    schedule = "MTWtFSs,11:40,13:35,Standard_DS1"
    patching = "Group0"
  }
}

