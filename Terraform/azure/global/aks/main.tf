# Run terraform init -backend-config="backend.private" to switch backend
terraform {
  backend "s3" {}
}

provider "azurerm" {
  subscription_id = "${var.subscription_id}"
  client_id       = "${var.client_id}"
  client_secret   = "${var.client_secret}"
  tenant_id       = "${var.tenant_id}"
}

resource "azurerm_resource_group" "rg_aks" {
  name     = "${var.resource_group_name}"
  location = "${var.location}"
}

resource "azurerm_kubernetes_cluster" "aks" {
  name                = "${var.cluster_name}"
  location            = "${azurerm_resource_group.rg_aks.location}"
  resource_group_name = "${azurerm_resource_group.rg_aks.name}"
  dns_prefix          = "${var.dns_prefix}"

  linux_profile {
    admin_username = "ubuntu"

    ssh_key {
      key_data = "${file("${path.module}/${var.ssh_public_key}")}"
    }
  }

  agent_pool_profile {
    name            = "default"
    count           = "${var.agent_count}"
    vm_size         = "Standard_DS2_v2"
    os_type         = "Linux"
    os_disk_size_gb = 30
  }

  service_principal {
    client_id     = "${var.client_id}"
    client_secret = "${var.client_secret}"
  }

  tags {
    Environment = "global"
  }
}

resource "azurerm_storage_account" "acrstorageaccount" {
  name                     = "adeweetmanacrsa"
  resource_group_name      = "${azurerm_resource_group.rg_aks.name}"
  location                 = "${azurerm_resource_group.rg_aks.location}"
  account_tier             = "Standard"
  account_replication_type = "GRS"
}

resource "azurerm_container_registry" "azurecr" {
  name                = "adeweetmanacr1"
  resource_group_name = "${azurerm_resource_group.rg_aks.name}"
  location            = "${azurerm_resource_group.rg_aks.location}"
  admin_enabled       = true
  sku                 = "Classic"
  storage_account_id  = "${azurerm_storage_account.acrstorageaccount.id}"
}

output "kube_config" {
  value = "${azurerm_kubernetes_cluster.aks.kube_config_raw}"
}

output "host" {
  value = "${azurerm_kubernetes_cluster.aks.kube_config.0.host}"
}

output "azure-cr_id" {
  value = "${azurerm_container_registry.azurecr.id}"
}

output "azure-cr_loginserver" {
  value = "${azurerm_container_registry.azurecr.login_server}"
}
