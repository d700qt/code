variable "subscription_id" {}
variable "client_id" {}
variable "client_secret" {}
variable "tenant_id" {}

variable "resource_group_name" {
  default = "rg_aks"
}

variable "location" {
  default = "westeurope"
}

variable "cluster_name" {
  default = "aks_cluster"
}

variable "dns_prefix" {
  default = "adeweetmanaks"
}

variable "ssh_public_key" {
  default = "ssh_public_key.pub"
}

variable "agent_count" {
  default = "1"
}
