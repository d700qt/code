$path = "C:\Users\adywe_000\.kube"
$kubectlFile = "azurek8s"

if (! (test-path -path $path)) {
    new-item -Path $path -Type Directory
}
$kubectlConfig = &terraform output kube_config
$kubectlConfig | out-file "C:\Users\adywe_000\.kube\$kubectlFile" -Encoding utf8
$env:KUBECONFIG=(join-path -path $path -ChildPath $kubectlFile)
kubectl get nodes
kubectl proxy
# start chrome  http://localhost:8001/api/v1/namespaces/kube-system/services/kubernetes-dashboard/proxy/#!/overview?namespace=default

