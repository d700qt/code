variable "subscription_id" {}
variable "client_id" {}
variable "client_secret" {}
variable "tenant_id" {}
variable "machine_name_prefix" {
    default = "adeweetman"
}

variable "pm_machine_name_suffix" {
        default = "pm1"
}

variable "machine_password" {}