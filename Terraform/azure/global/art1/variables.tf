variable "subscription_id" {}
variable "client_id" {}
variable "client_secret" {}
variable "tenant_id" {}
variable "machine_username" {
    default = "adeweetmanadmin"
}
variable "machine_password" {}
variable "machine_name_prefix" {
    default = "adeweetman"
}
variable "art_machine_name_suffix" {
     default = "art1"
}

variable "artifactory_download_filename" {
    default = "artifactory.zip"
}
