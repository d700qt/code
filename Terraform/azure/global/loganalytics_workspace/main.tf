/*
terraform {
  backend "local" {
    path = "terraform.tfstate"
  }
}
*/

# Run terraform init -backend-config="backend.private" to switch backend
terraform {
  backend "s3" {}
}

provider "azurerm" {
  subscription_id = "${var.subscription_id}"
  client_id       = "${var.client_id}"
  client_secret   = "${var.client_secret}"
  tenant_id       = "${var.tenant_id}"
}

resource "azurerm_resource_group" "rg-updates" {
  name     = "rg-updates"
  location = "West Europe"
}

resource "azurerm_log_analytics_workspace" "la-updates" {
  name                = "la-updates"
  location            = "${azurerm_resource_group.rg-updates.location}"
  resource_group_name = "${azurerm_resource_group.rg-updates.name}"
  sku                 = "PerGB2018"
  tags {
    environment = "global"
  }
}

