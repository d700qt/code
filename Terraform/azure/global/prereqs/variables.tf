variable "subscription_id" {}
variable "client_id" {}
variable "client_secret" {}
variable "tenant_id" {}
variable "pm_machine_name_suffix" {
        default = "pm1"
}