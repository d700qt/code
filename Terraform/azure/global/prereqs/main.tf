# terraform {
#   backend "local" {
#     path = "terraform.tfstate"
#   }
# }

# Run terraform init -backend-config="backend.private" to switch backend type
terraform {
  backend "s3" {}
}

provider "azurerm" {
  subscription_id = "${var.subscription_id}"
  client_id       = "${var.client_id}"
  client_secret   = "${var.client_secret}"
  tenant_id       = "${var.tenant_id}"
}

resource "azurerm_resource_group" "rg_terraform" {
  name     = "rg_terraform"
  location = "West Europe"
}

resource "azurerm_network_security_group" "nsg_terraform" {
  name                = "nsg_terraform"
  location            = "${azurerm_resource_group.rg_terraform.location}"
  resource_group_name = "${azurerm_resource_group.rg_terraform.name}"

  security_rule {
    name                       = "WinRM"
    priority                   = 100
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "5985"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }

  security_rule {
    name                       = "RDP"
    priority                   = 101
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "3389"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }

  security_rule {
    name                       = "http_bamboo"
    priority                   = 102
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "tcp"
    source_port_range          = "*"
    destination_port_range     = "8085"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }

  security_rule {
    name                       = "http_80"
    priority                   = 103
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "tcp"
    source_port_range          = "*"
    destination_port_range     = "80"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }

  security_rule {
    name                       = "SSH"
    priority                   = 104
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "22"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }

  security_rule {
    name                       = "HTTPS"
    priority                   = 105
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "443"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }

  security_rule {
    name                       = "http_artifactory"
    priority                   = 106
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "tcp"
    source_port_range          = "*"
    destination_port_range     = "8081"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }

  tags {
    environment = "global"
  }
}

resource "azurerm_virtual_network" "vnet_terraform" {
  name                = "vnet_terraform"
  address_space       = ["10.0.0.0/16"]
  location            = "${azurerm_resource_group.rg_terraform.location}"
  resource_group_name = "${azurerm_resource_group.rg_terraform.name}"
}

resource "azurerm_subnet" "subnet_terraform" {
  name                 = "subnet_terraform"
  resource_group_name  = "${azurerm_resource_group.rg_terraform.name}"
  virtual_network_name = "${azurerm_virtual_network.vnet_terraform.name}"
  address_prefix       = "10.0.2.0/24"
}

resource "azurerm_public_ip" "pip_ci1" {
  name                         = "pip_ci1"
  location                     = "${azurerm_resource_group.rg_terraform.location}"
  resource_group_name          = "${azurerm_resource_group.rg_terraform.name}"
  public_ip_address_allocation = "Static"
  idle_timeout_in_minutes      = 30
  domain_name_label            = "weetmana-ci1"

  tags {
    environment = "global"
  }
}

resource "azurerm_public_ip" "pip_art1" {
  name                         = "pip_art1"
  location                     = "${azurerm_resource_group.rg_terraform.location}"
  resource_group_name          = "${azurerm_resource_group.rg_terraform.name}"
  public_ip_address_allocation = "Static"
  idle_timeout_in_minutes      = 30
  domain_name_label            = "weetmana-art1"

  tags {
    environment = "global"
  }
}

resource "azurerm_public_ip" "pip_lnx1" {
  name                         = "pip_lnx1"
  location                     = "${azurerm_resource_group.rg_terraform.location}"
  resource_group_name          = "${azurerm_resource_group.rg_terraform.name}"
  public_ip_address_allocation = "Static"
  idle_timeout_in_minutes      = 30
  domain_name_label            = "weetmana-lnx1"

  tags {
    environment = "global"
  }
}

resource "azurerm_public_ip" "pip_dev1" {
  name                         = "pip_dev1"
  location                     = "${azurerm_resource_group.rg_terraform.location}"
  resource_group_name          = "${azurerm_resource_group.rg_terraform.name}"
  public_ip_address_allocation = "Static"
  idle_timeout_in_minutes      = 30
  domain_name_label            = "weetmana-dev1"

  tags {
    environment = "global"
  }
}

resource "azurerm_public_ip" "pip_win1" {
  name                         = "pip_win1"
  location                     = "${azurerm_resource_group.rg_terraform.location}"
  resource_group_name          = "${azurerm_resource_group.rg_terraform.name}"
  public_ip_address_allocation = "Static"
  idle_timeout_in_minutes      = 30
  domain_name_label            = "weetmana-win1"

  tags {
    environment = "uat"
  }
}

resource "azurerm_public_ip" "pip_win2" {
  name                         = "pip_win2"
  location                     = "${azurerm_resource_group.rg_terraform.location}"
  resource_group_name          = "${azurerm_resource_group.rg_terraform.name}"
  public_ip_address_allocation = "Static"
  idle_timeout_in_minutes      = 30
  domain_name_label            = "weetmana-win2"

  tags {
    environment = "prod"
  }
}

# resource "azurerm_public_ip" "pip_pe1" {
#   name                         = "pip_pe1"
#   location                     = "${azurerm_resource_group.rg_terraform.location}"
#   resource_group_name          = "${azurerm_resource_group.rg_terraform.name}"
#   public_ip_address_allocation = "Static"
#   idle_timeout_in_minutes      = 30
#   domain_name_label            = "weetmana-pe1"

#   tags {
#     environment = "global"
#   }
# }

output "ci1_public_ip_address" {
  value = "${azurerm_public_ip.pip_ci1.ip_address}"
}

output "ci1_public_fqdn" {
  value = "${azurerm_public_ip.pip_ci1.domain_name_label}.${azurerm_resource_group.rg_terraform.location}.cloudapp.azure.com"
}

output "art1_public_ip_address" {
  value = "${azurerm_public_ip.pip_art1.ip_address}"
}

output "art1_public_fqdn" {
  value = "${azurerm_public_ip.pip_art1.domain_name_label}.${azurerm_resource_group.rg_terraform.location}.cloudapp.azure.com"
}

output "dev1_public_ip_address" {
  value = "${azurerm_public_ip.pip_dev1.ip_address}"
}

output "dev1_public_fqdn" {
  value = "${azurerm_public_ip.pip_dev1.domain_name_label}.${azurerm_resource_group.rg_terraform.location}.cloudapp.azure.com"
}

output "win1_public_ip_address" {
  value = "${azurerm_public_ip.pip_win1.ip_address}"
}

output "win1_public_fqdn" {
  value = "${azurerm_public_ip.pip_win1.domain_name_label}.${azurerm_resource_group.rg_terraform.location}.cloudapp.azure.com"
}

output "win2_public_ip_address" {
  value = "${azurerm_public_ip.pip_win2.ip_address}"
}

output "win2_public_fqdn" {
  value = "${azurerm_public_ip.pip_win2.domain_name_label}.${azurerm_resource_group.rg_terraform.location}.cloudapp.azure.com"
}

output "lnx1_public_ip_address" {
  value = "${azurerm_public_ip.pip_lnx1.ip_address}"
}

output "lnx1_public_fqdn" {
  value = "${azurerm_public_ip.pip_lnx1.domain_name_label}.${azurerm_resource_group.rg_terraform.location}.cloudapp.azure.com"
}

# output "pe1_public_ip_address" {
#   value = "${azurerm_public_ip.pip_pe1.ip_address}"
# }


# output "pe1_public_fqdn" {
#   value = "${azurerm_public_ip.pip_pe1.domain_name_label}.${azurerm_resource_group.rg_terraform.location}.cloudapp.azure.com"
# }

