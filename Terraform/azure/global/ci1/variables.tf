variable "subscription_id" {}
variable "client_id" {}
variable "client_secret" {}
variable "tenant_id" {}
variable "machine_username" {
    default = "adeweetmanadmin"
}
variable "machine_password" {}
variable "machine_name_prefix" {
    default = "adeweetman"
}
variable "ci_machine_name_suffix" {
        default = "ci1"
}
variable "bamboo_installer_filename" {
    default = "atlassian-bamboo-6.6.3-windows-x64.exe"
}
